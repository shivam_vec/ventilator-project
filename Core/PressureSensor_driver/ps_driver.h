/*
* pressure sensure driver file
*
*/

#include "main.h"

/* Configure the Chip Select pins for all sensors*/
#define	PS1_port	GPIOB
#define	PS1_pin		GPIO_PIN_12

#define	PS2_port	GPIOA
#define	PS2_pin		GPIO_PIN_8

#define	PS3_port	GPIOC
#define	PS3_pin		GPIO_PIN_15

#define	PS4_port	GPIOC
#define	PS4_pin		GPIO_PIN_14

#define	PS5_port	GPIOC
#define	PS5_pin		GPIO_PIN_13

// Handle to the SPI instance that communicates with the sensors.
extern SPI_HandleTypeDef hspi2;

//Runs the startup sequence on all sensors.
void PS_Init(void);

//Turn off the CS on a particular sensor
void PS_ChipSelect_Set(uint8_t);

//Turn on the CS on a particular sensor.
void PS_ChipSelect_Reset(uint8_t);

//Read the requested sensor, covert raw value to PSI and return.
float StartReadPressure(uint8_t sensor);
float ReadPressure(uint8_t sensor);

// Caculate rate for particular interval
float Calrate(uint8_t a,uint8_t b);


/* TODO: Separate the conversion process to allow configuring of
 * output units.*/
//float ConvertPressure(float RawPressure);
