/*
* pressure sensor driver file
*
*/

#include "ps_driver.h"
#include "math.h"
#include "stdio.h"
#include "stdlib.h"

uint8_t TxData[4] = {0, 0, 0, 0};
uint8_t RxData[4] = {0, 0, 0, 0};
uint32_t Output = 0;

//Runs the startup sequence on all sensors.
void PS_Init(void){
	HAL_GPIO_WritePin(PS1_port, PS1_pin, GPIO_PIN_RESET);
	HAL_Delay(5);
	HAL_GPIO_WritePin(PS1_port, PS1_pin, GPIO_PIN_SET);
	HAL_Delay(5);
	HAL_GPIO_WritePin(PS2_port, PS2_pin, GPIO_PIN_RESET);
	HAL_Delay(5);
	HAL_GPIO_WritePin(PS2_port, PS2_pin, GPIO_PIN_SET);
	HAL_Delay(5);
	HAL_GPIO_WritePin(PS3_port, PS3_pin, GPIO_PIN_RESET);
	HAL_Delay(5);
	HAL_GPIO_WritePin(PS3_port, PS3_pin, GPIO_PIN_SET);
	HAL_Delay(5);
	HAL_GPIO_WritePin(PS4_port, PS4_pin, GPIO_PIN_RESET);
	HAL_Delay(5);
	HAL_GPIO_WritePin(PS4_port, PS4_pin, GPIO_PIN_SET);
	HAL_Delay(5);
	HAL_GPIO_WritePin(PS5_port, PS5_pin, GPIO_PIN_RESET);
	HAL_Delay(5);
	HAL_GPIO_WritePin(PS5_port, PS5_pin, GPIO_PIN_SET);
	HAL_Delay(5);
}

//Turn off the CS on a particular sensor
void PS_ChipSelect_Set(uint8_t sensor){
		if (sensor == 1) {HAL_GPIO_WritePin(PS1_port, PS1_pin, GPIO_PIN_SET);}
		else if (sensor == 2) {HAL_GPIO_WritePin(PS2_port, PS2_pin, GPIO_PIN_SET);}
		else if (sensor == 3) {HAL_GPIO_WritePin(PS3_port, PS3_pin, GPIO_PIN_SET);}
		else if (sensor == 4) {HAL_GPIO_WritePin(PS4_port, PS4_pin, GPIO_PIN_SET);}
		else {HAL_GPIO_WritePin(PS5_port, PS5_pin, GPIO_PIN_SET);}
}

//Turn on the CS on a particular sensor.
void PS_ChipSelect_Reset(uint8_t sensor){
		if (sensor == 1) {HAL_GPIO_WritePin(PS1_port, PS1_pin, GPIO_PIN_RESET);}
		else if (sensor == 2) {HAL_GPIO_WritePin(PS2_port, PS2_pin, GPIO_PIN_RESET);}
		else if (sensor == 3) {HAL_GPIO_WritePin(PS3_port, PS3_pin, GPIO_PIN_RESET);}
		else if (sensor == 4) {HAL_GPIO_WritePin(PS4_port, PS4_pin, GPIO_PIN_RESET);}
		else {HAL_GPIO_WritePin(PS5_port, PS5_pin, GPIO_PIN_RESET);}
}

//Read the requested sensor, covert raw value to PSI and return.
float StartReadPressure(uint8_t sensor){

	//step 1
	TxData[0] = 0xAA;
	PS_ChipSelect_Reset(sensor);
	HAL_Delay(1);
	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(&hspi2, &TxData, &RxData, 3, HAL_TIMEOUT);
	PS_ChipSelect_Set(sensor);
	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN  _12, GPIO_PIN_SET);

	/*//step2
	HAL_Delay(5);*/
}
float ReadPressure(uint8_t sensor)
{
	//step 3
	TxData[0] = 0xF0;
	PS_ChipSelect_Reset(sensor);
	HAL_Delay(1);
	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(&hspi2, &TxData, &RxData, 4, HAL_TIMEOUT);
	PS_ChipSelect_Set(sensor);
	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
	Output = RxData[1]<<16 | RxData[2]<<8 | RxData[3];
	return (((float)Output - 1677722)*15 )/(15099494 - 1677722);
}

// Calculate flow rate
float Calrate(uint8_t a,uint8_t b)
{
	float dia1 = 0.0217;
	float dia2 = 0.0055;
	float density = 1.225;
	float area = 3.14* (dia2 * dia2) / 4;
	float rate = 0.0;
	StartReadPressure(a);
	StartReadPressure(b);
	HAL_Delay(5);
	float PrDiff = ( ReadPressure(b)- ReadPressure(a)) ; // pressure difference in N/m2

	double velocity2 =0;

	if (PrDiff >= 0.003)
	{
	PrDiff = PrDiff * 6894.76 ;
	velocity2 = sqrt (2*PrDiff)/(density*(1-(dia2 * dia2)/(dia1*dia1)));
	}
	else { velocity2 =0; }

	rate = velocity2*area * 1000; //rate in lit/sec
	return (rate);

}
