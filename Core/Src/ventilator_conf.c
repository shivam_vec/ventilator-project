/*
 * ventilator_conf.c
 *
 *  Created on: 12-Jun-2020
 *      Author: viplo
 */

#include "ventilator_conf.h"
#include "BL_driver.h"
#include <stdlib.h>
#include <string.h>

/*
 * Commands for updating ventilator parameters
*/
/*=============START===============*/
void Set_CircuitPr(float newValue){
	CircuitPr=newValue;
}

void Set_LungPr(float newValue){
	LungPr=newValue;
}

void Set_MaxPr(float newValue){
	MaxPr=newValue;
}

void Set_PEEP(float newValue){
	Peep=newValue;
}

void Set_BreathRate(float newValue){
	BreathRate=newValue;
}

void Set_InspirationTime(float newValue){
	InspirationTime=newValue;
}

void Set_TidalVolume(float newValue){
	TidalVolume=newValue;
}
/*=============END===============*/

/*
 * Transmit the current values of all Parameters.
*/
void Show_Parameters(void){
	BL_transmit("Circuit Pressure",CircuitPr);
	BL_transmit("Lung Pressure",LungPr);
	BL_transmit("Max Pressure",MaxPr);
	BL_transmit("Peep",Peep);
	BL_transmit("Breathe Rate",BreathRate);
	BL_transmit("Inspiration Time",InspirationTime);
	BL_transmit("Tidal Volume",TidalVolume);
}

/*
 * Transmit the values of all Pressure vaiables
*/
void Pressure_Sensor_Values(void){
	BL_transmit("PS1",PressureOut1);
	BL_transmit("PS2",PressureOut2);
	BL_transmit("PS3",PressureOut3);
	BL_transmit("PS4",PressureOut4);
	BL_transmit("PS5",PressureOut5);
}

/*
 * Parses the serial input from Bluetooth as per the following table-
 *
 * Task						Command		Param
 * (Update tasks)
 * Circuit Pressure: 		CP			float (psi)
 * Lung Pressure:			LP			float (psi)
 * Max Pressure:			MP			float (psi)
 * PEEP:					PE			float
 * Breath Rate:				BR			float (Breaths per minute)
 * Inspiration time:		IT			float (seconds)
 * Tidal Volume:			TV			float (mL)
 *
 * (Display Tasks)
 * All Parameters:			SH			void
 * Pressure Sensor values:	PS			void
 * ----------------------------------------------
 *
 * Command structure- 7 characters in total with following structure:
 * CP014.5 (last 5 characters corresponding to the new value)
 * SHxxxxx (only the command code matters.)
*/
void Parse_BL_Input(unsigned char *buffer){
	char command[2]={0};
	char params[8]={0};
	memcpy(command,buffer,2);
	memcpy(params,&buffer[2],5);

	if (strcmp(command,"CP")==0){
		Set_CircuitPr(atof(params));
	}
	else if (strcmp(command,"LP")==0){
		Set_LungPr(atof(params));
	}
	else if (strcmp(command,"MP")==0){
		Set_MaxPr(atof(params));
	}
	else if (strcmp(command,"PE")==0){
		Set_PEEP(atof(params));
	}
	else if (strcmp(command,"BR")==0){
		Set_BreathRate(atof(params));
	}
	else if (strcmp(command,"IT")==0){
		Set_InspirationTime(atof(params));
	}
	else if (strcmp(command,"TV")==0){
		Set_TidalVolume(atof(params));
	}
	else if (strcmp(command,"SH")==0){
		Show_Parameters();
	}
	else if (strcmp(command,"PS")==0){
		Pressure_Sensor_Values();
	}
	else{
		BL_transmit("Illegal command",0);
	}
}
