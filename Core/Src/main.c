/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include "ventilator_conf.h"
#include <string.h>
#include "ps_driver.h"
#include "SM_driver.h"
#include "led.h"
#include "bldc_driver.h"
#include "BL_driver.h"
#include "usb_driver.h"
#include "parameters.h"
#include "stdbool.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

//p11 connector defines
#define SolenoidPort1	GPIOB
#define SolenoidPin1	GPIO_PIN_3

//p8 connector defines
#define SolenoidPort2	GPIOB
#define SolenoidPin2	GPIO_PIN_4
//p9 connector defines
#define SolenoidPort3	GPIOB
#define SolenoidPin3	GPIO_PIN_5

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi2;

TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI2_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

// fsm states

uint8_t FSM_State = 1;
uint8_t LedNumber = 1;
unsigned char myTXdata[] = "\r\nEnter TidalVolume: ";

//pressure variables
float PressureOut1 =0;
float PressureOut2 =0;
float PressureOut3 =0;
float PressureOut4 =0;
float PressureOut5 =0;

float InitialPr5 = 0;
float InitialPr3 = 0;
int32_t	PrConverted5 = 0;
int32_t PrConverted3 = 0;
//uart buffer
unsigned char BleBuff[50] = {0};
unsigned char BleReceiveBuff[50] = {0};

//usb buffer
unsigned char USBBuff[50] ={0};
unsigned char USBReceiveBuff[50] = {0};

//parameters
float CircuitPr = 14.5;
float LungPr = 14;
float MaxPr = 15.5;
float Peep = 5;
float BreathRate = 30;
float InspirationTime = 2;
float TidalVolume = 500;
float Volume = 0.0;
float Rate = 0.0;
float PIP ;
float diff;
float ie = 1/4;


// Mode (0= Volume Mode ; 1 = Pressure Mode)

bool Mode = 0;

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
/*float CDC_Recieve_FS_CallBack(uint8_t *buff, uint32_t len)
{
	//TODO:Implement usb receive
	float value[0] = {0};
	memcpy(value, buff, len);
    TidalVolume = value[0];
	//return val;
}*/
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI2_Init();
  MX_TIM3_Init();
  MX_USART1_UART_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */

  //Init BLDC with timer instance, channel, and pulse width corresponding
  //to minimum rpm and maximum rpm, respectively. ESC frequency at 50Hz.
  BLDCInit(&htim3,TIM_CHANNEL_4,10,20);

  //Init Stepper motor with 'step-size' and 'maximum-angle' allowed
  SMInit(1.8,180.0);

  //Run the Startup sequence on all Pressure sensors.
  PS_Init();

  //Make the Bluetooth ready to receive in Interrupt mode.
 // BL_Receive();
  //Calibration sequence
//  PressureOut5 = ReadPressure(5);  //ic 9 output
//  HAL_Delay(10);
//  PressureOut5+=ReadPressure(5);
//  HAL_Delay(10);
//  PressureOut5+=ReadPressure(5);
//  HAL_Delay(10);
//  InitialPr5 = PressureOut5/10;
//
//  PressureOut3 = ReadPressure(3);  //ic 8 output
//  HAL_Delay(10);
//  PressureOut3+=ReadPressure(3);
//  HAL_Delay(10);
//  PressureOut3+=ReadPressure(3);
//  HAL_Delay(10);
//  InitialPr3 = PressureOut3/10;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

// uint32_t InitTicks = 0;
 // uint32_t Ticks	= 0;
  uint32_t InitialTime = 0;
  uint32_t Time	= 0;
  uint32_t InitTi = 0;
  uint32_t Ti = 0;
  uint32_t InitTe = 0;
  uint32_t Te = 0;
  float    CalTe = 0;
 // uint8_t myTXdata[] = "\r\nEnter TidalVolume: ";
  while (1)
  {
	  //Read Pressure Sensor Values
	  StartReadPressure(1);
	  StartReadPressure(2);
	  StartReadPressure(3);
	  StartReadPressure(4);
	  StartReadPressure(5);
	  HAL_Delay(5);
	  PressureOut1 = ReadPressure(1);
	  PressureOut2 = ReadPressure(2);
	  PressureOut3 = ReadPressure(3);  //in use
	  PressureOut4 = ReadPressure(4); //ic 8 output
	  PressureOut5 = ReadPressure(5);  //ic 9 output //
	  /*convert to cm of h20*/
	  PrConverted3 = (int32_t)((PressureOut3 - PressureOut4) *70.3);
	  diff = (PressureOut5 - PressureOut4);
	  PrConverted5 = (int32_t)((PressureOut5 - PressureOut4) *70.3);
      PIP = 20;


//	  // USB
  //USB_Transmit("PS1",PressureOut1,", ");
	 //HAL_Delay(5);
//	  USB_Transmit("PS2",PressureOut2,", ");
//	  HAL_Delay(5);
//	  USB_Transmit("PS3",PressureOut3,", ");
//	  HAL_Delay(5);
//	  USB_Transmit("PS4",PressureOut4,", ");
//	  HAL_Delay(5);

	 // USB_Transmit("PS5",PressureOut5,"\n\r");
     HAL_UART_Transmit(&huart1, myTXdata , strlen(myTXdata), 1000);

	  // turn on solenoid whenever stepper motor is in zero position
	 if (HAL_GPIO_ReadPin(SM_port,HallSense) == GPIO_PIN_SET){
		 HAL_GPIO_WritePin(SolenoidPort2, SolenoidPin2, GPIO_PIN_SET);
	 }
	 else{
		 HAL_GPIO_WritePin(SolenoidPort2, SolenoidPin2, GPIO_PIN_RESET);
	 }

	switch (FSM_State)
    {
	case 1:
        //set parameters tidal volume, BPM, Inspiration time
     // USB_Transmit_userinput("Enter Tidal Volume");
         // TidalVolume = CDC_Recieve_FS_CallBack(USBReceiveBuff, strlen(USBReceiveBuff)+1);
    	 // sprintf(":%.2f", USBReceiveBuff);*/

    	  FSM_State = 2;
    	  break;
      case 2:
        //start inspiration and monitor lung pressure
        //start inspiration
    	  Volume = 0;
    	  InitTi = HAL_GetTick();
    	 //HAL_GPIO_WritePin(SolenoidPort1, SolenoidPin1, GPIO_PIN_SET);
    	 for (int i =0; i<5; i++)
    	     	 {
    	     		 InitialTime = HAL_GetTick();
    	     		 if (i ==0) {HAL_GPIO_WritePin(SolenoidPort1, SolenoidPin1, GPIO_PIN_SET);}
    	     		 MoveSteps(10, CCW, SM_STEPDELAY_CCW);
    	     		//if (i ==0) {HAL_GPIO_WritePin(SolenoidPort1, SolenoidPin1, GPIO_PIN_RESET);}
    	     		 Rate = Calrate(3,5);  //calculating flow rate
    	     		 Time = HAL_GetTick()- InitialTime;
    	     		 InitialTime = HAL_GetTick();
    	     		 Volume+= Rate*Time;
    	     		 if (Mode ==0 && Volume > TidalVolume)
    	     		  {
    	     		     HAL_GPIO_WritePin(SolenoidPort3, SolenoidPin3, GPIO_PIN_SET);
    	     		   }
    	     	 }
    	// HAL_GPIO_WritePin(SolenoidPort1, SolenoidPin1, GPIO_PIN_RESET);
    	// InitialTime = HAL_GetTick();
    	 Ti = HAL_GetTick() - InitTi;
    	 Rate = 0;
    	 FSM_State = 4;
       //read pressure
       //PressureOut1 = ReadPressureRaw(1);

    	 break;
      case 3:
        //bypass

      case 4:
        //monitor tidal volume
        //if (TidalIn > TidalVolume){FSM_State=5;}
    	 // HAL_Delay(2000);
    	//  FSM_State = 5;



    	      if (Mode == 0 && (Volume > TidalVolume || PrConverted5 > PIP) )
    	      	    {
    	      		  FSM_State = 5;
    	      		  HAL_GPIO_WritePin(SolenoidPort3, SolenoidPin3, GPIO_PIN_SET);
    	      	    }

    	      else if ((Mode == 1) && (PrConverted5 > PIP))
    	    	 {
    	    		 FSM_State = 5;
    	    		 HAL_GPIO_WritePin(SolenoidPort3, SolenoidPin3, GPIO_PIN_SET);

    	    	 }
    	      else
    	           {
    	          	   HAL_Delay(250);
    	          	   FSM_State= 5;
    	          	 }

               //  Ti = HAL_GetTick() - InitTi;
    	  break;
      case 5:
        //stop inspiration and start expiration
    	  InitTe = HAL_GetTick();
    	  MoveSteps(SM_MAXSTEPS, CW, SM_STEPDELAY_CW); //move stepper motor to close valve
  		  HAL_GPIO_WritePin(SolenoidPort3, SolenoidPin3, GPIO_PIN_RESET);
  		  HAL_GPIO_WritePin(SolenoidPort1, SolenoidPin1, GPIO_PIN_RESET);
    	  FSM_State = 7;
    	 // Te = HAL_GetTick() - InitTe;
    	  //InitTicks = HAL_GetTick();
    	  CalTe = Ti * 3;
    	  break;
      case 6:
        //monitor lung pressure
        //read pressure
        //PressureOut1 = ReadPressureRaw(1);
        //if (PressureOut1 < Peep){FSM_State=7;}

      case 7:
        //stop expiration and start inspiration again
    	  //Ticks = HAL_GetTick() - InitTicks; // time passed since last state
    	  Te = HAL_GetTick() - InitTe;
    	  if (Te >= CalTe)
         {
    	  FSM_State = 2;
    	  break;}
    	  break;
      default:
     	;
    }

//	switch (LedNumber)
//	    {
//	      case 1:
//	    	  LED1_ON();
//	        LedNumber=2;
//	        break;
//	      case 2:
//	    	  LED2_ON();
//	        LedNumber=3;
//	        break;
//	      case 3:
//	    	  LED3_ON();
//	        LedNumber=4;
//	        break;
//	      case 4:
//	    	  LED4_ON();
//	        LedNumber=1;
//	        break;
//	      default:
//	      		;
//	    }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV2;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 96;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1000;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1|GPIO_PIN_2, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8|GPIO_PIN_15, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6 
                          |GPIO_PIN_7|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pins : PC13 PC14 PC15 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PA1 PA2 PA8 PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_8|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PA5 */
  GPIO_InitStruct.Pin = GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB12 PB3 PB4 PB5 
                           PB6 PB7 PB8 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5 
                          |GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	//Parse the received command
	Parse_BL_Input(BleReceiveBuff);

	//Flush the Register to prevent Buffer overrun, in case
	//command longer than 7 characters is sent.
	__HAL_UART_FLUSH_DRREGISTER(huart);

	//Start receiving again.
	BL_Receive();
}


//void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart){
//	HAL_UART_Transmit(huart,"buffer overrun.",15,HAL_TIMEOUT);
//	__HAL_UART_CLEAR_OREFLAG(huart);
////	__HAL_UART_ENABLE(huart);
////	__HAL_UART_FLUSH_DRREGISTER(huart);
//	HAL_UART_StateTypeDef state = HAL_UART_GetState(huart);
//	uint32_t error = HAL_UART_GetError(huart);
//}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
