/*
 * ventilator_conf.h
 *
 *  Created on: 12-Jun-2020
 *      Author: viplo
 */

#ifndef INC_VENTILATOR_CONF_H_
#define INC_VENTILATOR_CONF_H_

#include "main.h"

/* Ventilator parameters*/
extern float CircuitPr;
extern float LungPr;
extern float MaxPr;
extern float Peep;
extern float BreathRate;
extern float InspirationTime;
extern float TidalVolume;

extern float PressureOut1;		//Pressure Sensor 1 output
extern float PressureOut2;		//Pressure Sensor 2 output
extern float PressureOut3;		//Pressure Sensor 3 output
extern float PressureOut4;		//Pressure Sensor 4 output
extern float PressureOut5;		//Pressure Sensor 5 output

/*
 * Commands for updating ventilator parameters
*/
/*============================*/
void Set_CircuitPr(float newValue);
void Set_LungPr(float newValue);
void Set_MaxPr(float newValue);
void Set_PEEP(float newValue);
void Set_BreathRate(float newValue);
void Set_InspirationTime(float newValue);
void Set_TidalVolume(float newValue);
/*============================*/


/*
 * Transmit the current values of all Parameters.
*/
void Show_Parameters(void);

/*
 * Transmit the values of all Pressure vaiables
*/
void Pressure_Sensor_Values(void);

/*
 * Parses the serial input from Bluetooth as per the following table-
 *
 * Task						Command		Param
 * (Update tasks)
 * Circuit Pressure: 		CP			float (psi)
 * Lung Pressure:			LP			float (psi)
 * Max Pressure:			MP			float (psi)
 * PEEP:					PE			float
 * Breath Rate:				BR			float (Breaths per minute)
 * Inspiration time:		IT			float (seconds)
 * Tidal Volume:			TV			float (mL)
 *
 * (Display Tasks)
 * All Parameters:			SH			void
 * Pressure Sensor values:	PS			void
 * ----------------------------------------------
 *
 * Command structure- 7 characters in total with following structure:
 * CP014.5 (last 5 characters corresponding to the new value)
 * SHxxxxx (only the command code matters.)
*/
void Parse_BL_Input(unsigned char *buffer);

#endif /* INC_VENTILATOR_CONF_H_ */
