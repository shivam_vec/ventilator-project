/*
 * parameters.h
 *
 *  Created on: Jul 4, 2020
 *      Author: Dell
 */

#ifndef INC_PARAMETERS_H_
#define INC_PARAMETERS_H_

//stepprt motor motion parameters

#define SM_MAXSTEPS 		50		//maximum steps in counter clockwise direction
#define SM_STEPDELAY_CAL	15	    //time delay between 2 steps in mS during calibration
#define SM_STEPDELAY_CW		5	    //time delay between 2 steps in mS in clockwise direction
#define SM_STEPDELAY_CCW	8	    //time delay between 2 steps in mS in counter clockwise direction

//
#define DWELL_TIME		5000	//dwell time parameter in milli seconds

//


#endif /* INC_PARAMETERS_H_ */
