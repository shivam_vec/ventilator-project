/*
 * usb_driver.c
 *
 *  Created on: 19-Jun-2020
 *      Author: viplo
 */

#include "usb_driver.h"
#include <string.h>
#include <float.h>

/*
 *	Transmits a key-value pair after converting
 *	it to the following format-
 *	"{key}:{value}\n"
*/
void USB_Transmit(char key[], float value, char lineEnding[]){
	CDC_Transmit_FS((uint8_t*)key, strlen(key));
	HAL_Delay(5);
	int final = (int)(value*100);
	sprintf(USBBuff, ": %i",final);
	CDC_Transmit_FS((uint8_t*)USBBuff, strlen(USBBuff));
	HAL_Delay(5);
	CDC_Transmit_FS((uint8_t*)lineEnding, strlen(lineEnding));
}

/*
 * Starts listening for a Bluetooth command under
 * interrupt mode. Receiving is complete when exactly
 * 7 bytes have been received.
*/


void USB_Transmit_userinput(char key[])
{
	CDC_Transmit_FS((uint8_t*)key, strlen(key));
	sprintf(USBBuff, ":");
}
