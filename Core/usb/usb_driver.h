/*
 * usb_driver.h
 *
 *  Created on: 19-Jun-2020
 *      Author: viplo
 */

#ifndef USB_USB_DRIVER_H_
#define USB_USB_DRIVER_H_

#include "main.h"

//UART Buffer for Transmitting
extern unsigned char USBBuff[50];

//UART Buffers for Receiving
extern unsigned char USBReceiveBuff[50];

/*
 *	Transmits a key-value pair after converting
 *	it to the following format-
 *	"{key}:{value}\n"
*/
void USB_Transmit(char key[], float value, char lineEnding[]);

/*
 * Starts listening for a Bluetooth command under
 * interrupt mode. Receiving is complete when exactly
 * 7 bytes have been received.
*/

void USB_Transmit_userinput(char key[]);
#endif /* USB_USB_DRIVER_H_ */
