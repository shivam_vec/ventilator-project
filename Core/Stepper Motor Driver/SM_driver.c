/*
* stepper motor  driver file
*
*/

#include "SM_driver.h"
#include <stdbool.h>
#include <stdlib.h>
#include "parameters.h"

static bool CheckZero(void);

//Returns true if the Stepper Motor is at Zero position.
static bool CheckZero(void){
	bool retval = (HAL_GPIO_ReadPin(SM_port,HallSense) == GPIO_PIN_SET);
	return retval;
}

/*
 * Configure the step-size and the maximum angle allowed.
 * Return the motor to its zero position.
*/
void SMInit(float step_size,float max_angle){
	StepSize=step_size;
	MaxAngle=max_angle;

	/*
	 * We need to make sure that we are at the 0 position when we start.
	 *
	 * Warning: This is not recommended to be used on the final product as it risks
	 * momentarily closing the valves in case the system reboots for some reason.
	*/
	SMHome();
}

/*
 * Return to position zero.
*/
void SMHome(void){
	/*This implementation assumes that Global variable might be out of sync and not reliable*/
	int maxAttempts = (int)(MaxAngle/StepSize);
	for(int i=0;i<maxAttempts;i++){
		MoveSteps(1,CW, SM_STEPDELAY_CAL);
		if (CheckZero()){
			break;
		}
	}
	CurrentAngle=0;
}

/*
 * Move one step. Gives one pulse to the Step pin of the motor driver.
*/
void OneStep(uint8_t DelayTime){
	HAL_GPIO_WritePin(SM_port,Step_pin,GPIO_PIN_SET);
	HAL_Delay(DelayTime);
	HAL_GPIO_WritePin(SM_port,Step_pin,GPIO_PIN_RESET);
	HAL_Delay(DelayTime);
}

/*
 * Move a given number of steps in the requested direction.
 * Updates the CurrentAngle variable to keep it in sync with
 * the real orientation. Also makes sure that the maximum and
 * minimum angle bounds are never broken.
*/
void MoveSteps(uint8_t steps, uint8_t direction, uint8_t StepDelay){
	int8_t multiplier=0;
	if (direction == CW) {
		/* write direction pin to set direction to clockwise*/
		HAL_GPIO_WritePin(SM_port,Dir_pin,GPIO_PIN_SET);
		multiplier=1;
	}
	else {
		/* clear direction pin to set direction to counter clockwise */
		HAL_GPIO_WritePin(SM_port,Dir_pin,GPIO_PIN_RESET);
		multiplier=-1;
	}

	for (uint8_t i=1; i<=steps; i++){
		if ((CurrentAngle+ (multiplier*StepSize)) > 0 || (CurrentAngle+ (multiplier*StepSize)) < MaxAngle){
			OneStep(StepDelay);
			CurrentAngle += multiplier*StepSize;
		}
	}
}

/*
 * Get the stepper-motor to a particular angle, while remaining within
 * allowed angle range.
*/
void SetAngle(float angle){
	//Ensure that requested angle is within the acceptable range.
	if (angle<0){angle=0;}
	else if(angle>MaxAngle){angle=MaxAngle;}

	//Calculate the number of steps to move.
	float displacement = angle-CurrentAngle;
	uint8_t steps = (uint8_t) abs((int)(displacement/StepSize));

	//Call move steps based on whether to turn CW or CCW.
	if (displacement >= 0){
		MoveSteps(steps,CW, SM_STEPDELAY_CW);
	}
	else{
		MoveSteps(steps,CCW, SM_STEPDELAY_CCW);
	}
}
