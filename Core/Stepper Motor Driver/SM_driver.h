/*
* stepper motor  driver file
*
*/

#include "main.h"

#define	CW	1	//clock wise
#define	CCW	0	//counter clockwise

//Define the step and direction ports and pins for Stepper Motor.
#define	SM_port		GPIOA
#define	Step_pin	GPIO_PIN_1
#define Dir_pin		GPIO_PIN_2

//Define Hall sensor input pin. The sensor is High when stepper-motor is at zero position.
#define HallSense	GPIO_PIN_5

//Instance and State variables
float CurrentAngle;			//Stores the current position of the stepper-motor
float StepSize;				//Step-size of the stepper-motor in degrees
float MaxAngle;				//Maximum angle that the stepper-motor is allowed to rotate

/*
 * Configure the step-size and the maximum angle allowed.
 * Return the motor to its zero position.
*/
void SMInit(float step_size,float max_angle);

/*
 * Return to position zero.
*/
void SMHome(void);

/*
 * Move one step. Gives one pulse to the Step pin of the motor driver.
*/
void OneStep(uint8_t DelayTime);

/*
 * Move a given number of steps in the requested direction.
 * Updates the CurrentAngle variable to keep it in sync with
 * the real orientation. Also makes sure that the maximum and
 * minimum angle bounds are never broken.
*/
void MoveSteps(uint8_t steps, uint8_t direction, uint8_t StepDelay);

/*
 * Get the stepper-motor to a particular angle, while remaining within
 * allowed angle range.
*/
void SetAngle(float angle);
