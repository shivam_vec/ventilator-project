///*
// * bldc_driver.c
// *
// *  Created on: 10-Jun-2020
// *      Author: viplo
// */
//
//
#include "bldc_driver.h"

//Set the instance variables. The pulse widths corresponding to minimum rpm and maximum rpm are required.
//It is assumed that the ESC operates at 50Hz. For any other frequency, we will need to update parameters
//as needed.
void BLDCInit(TIM_HandleTypeDef *bldc_htim, uint32_t bldc_channel, float min_rpm, float max_rpm){
	assert_param(IS_TIM_CHANNELS(Channel));

	htim = *bldc_htim;
	channel = bldc_channel;

	MaxRpm = (int)max_rpm*10;
	MinRpm = (int)min_rpm*10;
}

//Start PWM and turn on the BLDC
HAL_StatusTypeDef BLDCStart(void){
	return HAL_TIM_PWM_Start(&htim,channel);
}

//Stop the PWM.
HAL_StatusTypeDef BLDCStop(void){
	return HAL_TIM_PWM_Stop(&htim,channel);
}

//Set duty-cycle in %. 0% duty-cycle corresponds to minimum rpm.
void BLDCSet(float dutycycle){
	uint32_t compare;

	//Make sure that the duty-cycle is within 0 to 100 range
	if (dutycycle < 0) {dutycycle=0;}
	else if (dutycycle > 100) {dutycycle=100;}

	//Calculate the value of compare register of the timer.
	compare = (uint32_t)(MinRpm + (dutycycle/100)*(MaxRpm-MinRpm));
	__HAL_TIM_SET_COMPARE(&htim,channel,compare);
}
