/*
 * bldc_driver.h
 *
 *  Created on: 10-Jun-2020
 *      Author: viplo
 */

#ifndef BLDC_ESC_BLDC_DRIVER_H_
#define BLDC_ESC_BLDC_DRIVER_H_

#include "main.h"

//Instance variables
TIM_HandleTypeDef htim;		//Handle of the PWM timer
uint32_t channel;			//Channel on which PWM is configured
int MaxRpm;					//Pulse width corresponding to maximum RPM
int MinRpm;					//Pulse width corresponding to minimum RPM

//Set the instance variables. The pulse widths corresponding to minimum rpm and maximum rpm are required.
//It is assumed that the ESC operates at 50Hz. For any other frequency, we will need to update parameters
//as needed.
void BLDCInit(TIM_HandleTypeDef *bldc_htim, uint32_t bldc_channel, float max_rpm, float min_rpm);

//Start PWM and turn on the BLDC
HAL_StatusTypeDef BLDCStart(void);

//Stop PWM and turn off the BLDC
HAL_StatusTypeDef BLDCStop(void);

//Set duty-cycle in %. 0% duty-cycle corresponds to minimum rpm.
void BLDCSet(float dutycycle);

#endif /* BLDC_ESC_BLDC_DRIVER_H_ */
